import { AppMain } from '@/views/layout'
export default [
  {
    path: '',
    name: '仓储管理',
    meta: { title: '仓储管理', icon: 'component', roles: ['admin'] },
    component: (resolve) => require(['@/views/layout/Layout'], resolve),
    type: 1,
    children: [
      {
        path: '',
        component: AppMain,
        name: '入库',
        meta: { title: '入库', icon: 'component', roles: ['admin'] },
        type: 2,
        children: [
          {
            path: 'asnExcelImp',
            component: (resolve) => require(['@/views/inbound/asnExcelImp'], resolve),
            name: '君马ASN订单导入',
            meta: { title: '君马ASN订单导入', icon: 'component', roles: ['admin'] },
            type: 3
          },
          {
            path: 'inboundExcelImp',
            component: (resolve) => require(['@/views/inbound/inboundExcelImp'], resolve),
            name: '入库通知单导入',
            meta: { title: '入库通知单导入', icon: 'component', roles: ['admin'] },
            type: 3
          },
          {
            path: 'inboundNotice',
            component: (resolve) => require(['@/views/inbound/inboundNotice'], resolve),
            name: '入库通知单查询',
            meta: { title: '入库通知单查询', icon: 'component', roles: ['admin'] },
            type: 3
          },
          {
            path: 'inboundInspect',
            component: (resolve) => require(['@/views/inbound/inboundInspect'], resolve),
            name: '入库质检',
            meta: { title: '入库质检', icon: 'component', roles: ['admin'] },
            type: 3
          },
          {
            path: 'inboundPutaway',
            component: (resolve) => require(['@/views/inbound/inboundPutaway'], resolve),
            name: '入库作业',
            meta: { title: '入库作业', icon: 'component', roles: ['admin'] },
            type: 3
          },
          {
            path: 'inboundRecord',
            component: (resolve) => require(['@/views/inbound/inboundRecord'], resolve),
            name: '入库记录',
            meta: { title: '入库记录', icon: 'component', roles: ['admin'] },
            type: 3
          },
          {
            path: 'inboundkey',
            component: (resolve) => require(['@/views/inbound/inboundKey'], resolve),
            name: '入库钥匙管理',
            meta: { title: '入库钥匙管理', icon: 'component', roles: ['admin'] },
            type: 3
          }
        ]
      },
      {
        path: '',
        component: AppMain,
        name: '出库',
        meta: { title: '出库', icon: 'component', roles: ['admin'] },
        type: 2,
        children: [
          {
            path: 'outboundExcelImp',
            component: (resolve) => require(['@/views/outbound/outboundExcelImp'], resolve),
            meta: { title: '出库通知单导入', icon: 'component', roles: ['admin'] },
            name: '出库通知单导入',
            type: 3
          },
          {
            path: 'outboundNotice',
            component: (resolve) => require(['@/views/outbound/outboundNotice'], resolve),
            name: '出库通知单查询',
            meta: { title: '出库通知单查询', icon: 'component', roles: ['admin'] },
            type: 3
          },
          {
            path: 'outboundPrepare',
            component: (resolve) => require(['@/views/outbound/outboundPrepare'], resolve),
            name: '出库备料',
            meta: { title: '出库备料', icon: 'component', roles: ['admin'] },
            type: 3
          },
          {
            path: 'outboundKeyPrepare',
            component: (resolve) => require(['@/views/outbound/outboundKeyPrepare'], resolve),
            name: '出库钥匙备料',
            meta: { title: '出库钥匙备料', icon: 'component', roles: ['admin'] },
            type: 3
          },
          {
            path: 'outboundShip',
            component: (resolve) => require(['@/views/outbound/outboundShip'], resolve),
            name: '出库作业',
            meta: { title: '出库作业', icon: 'component', roles: ['admin'] },
            type: 3
          },
          {
            path: 'outboundRecord',
            component: (resolve) => require(['@/views/outbound/outboundRecord'], resolve),
            name: '出库记录',
            meta: { title: '出库记录', icon: 'component', roles: ['admin'] },
            type: 3
          },
          {
            path: 'queryShipRecordList',
            component: (resolve) => require(['@/views/outbound/queryShipRecordList'], resolve),
            name: '发运作业',
            meta: { title: '发运作业', icon: 'component', roles: ['admin'] },
            type: 3
          },
          {
            path: 'queryNewChannelReleaseList',
            component: (resolve) => require(['@/views/outbound/queryNewChannelReleaseList'], resolve),
            name: '二手车发运',
            meta: { title: '二手车发运', icon: 'component', roles: ['admin'] },
            type: 3
          }
        ]
      },
      {
        path: '',
        component: AppMain,
        name: '库存',
        meta: { title: '库存', icon: 'component', roles: ['admin'] },
        type: 2,
        children: [
          {
            path: 'stockQuery',
            component: (resolve) => require(['@/views/stock/stockQuery'], resolve),
            name: '库存管理',
            meta: { title: '库存管理', icon: 'component', roles: ['admin'] },
            type: 3
          },
          {
            path: 'initStock',
            component: (resolve) => require(['@/views/stock/initStock'], resolve),
            name: '期初库存',
            meta: { title: '期初库存', icon: 'component', roles: ['admin'] },
            type: 3
          },
          {
            path: 'adjustStock',
            component: (resolve) => require(['@/views/stock/adjustStock'], resolve),
            name: '库存调整',
            meta: { title: '库存调整', icon: 'component', roles: ['admin'] },
            type: 3
          },
          {
            path: 'dailyStockQuery',
            component: (resolve) => require(['@/views/stock/dailyStockQuery'], resolve),
            name: '日库存查询',
            meta: { title: '日库存查询', icon: 'component', roles: ['admin'] },
            type: 3
          },
          {
            path: 'inboundMovement',
            component: (resolve) => require(['@/views/stock/inboundMovement'], resolve),
            name: '库位调整记录',
            meta: { title: '库位调整记录', icon: 'component', roles: ['admin'] },
            type: 3
          }
        ]
      },
      {
        path: '',
        component: AppMain,
        name: '数据维护',
        meta: { title: '数据维护', icon: 'component', roles: ['admin'] },
        type: 2,
        children: [
          {
            path: 'reInboundToOTM',
            component: (resolve) => require(['@/views/dataMaintain/reInboundToOTM'], resolve),
            name: '补发入库数据',
            meta: { title: '补发入库数据', icon: 'component', roles: ['admin'] },
            type: 3
          },
          {
            path: 'reShipmentToOTM',
            component: (resolve) => require(['@/views/dataMaintain/reShipmentToOTM'], resolve),
            name: '补发发运数据',
            meta: { title: '补发发运数据', icon: 'component', roles: ['admin'] },
            type: 3
          },
          {
            path: 'cleanInboundToOTM',
            component: (resolve) => require(['@/views/dataMaintain/cleanInboundToOTM'], resolve),
            name: '清理库存',
            meta: { title: '清理库存', icon: 'component', roles: ['admin'] },
            type: 3
          }]
      },
      {
        path: '',
        component: AppMain,
        name: '合格证',
        meta: { title: '合格证', icon: 'component', roles: ['admin'] },
        type: 2,
        children: [
          {
            path: 'certificateMgt',
            component: (resolve) => require(['@/views/certificate/certificateMgt'], resolve),
            name: '合格证管理',
            meta: { title: '合格证管理', icon: 'component', roles: ['admin'] },
            type: 3
          },
          {
            path: 'certificateQuery',
            component: (resolve) => require(['@/views/certificate/certificateQuery'], resolve),
            name: '合格证列表查询',
            meta: { title: '合格证列表查询', icon: 'component', roles: ['admin'] },
            type: 3
          }]
      },
      {
        path: '',
        component: AppMain,
        name: '工厂未备料',
        meta: { title: '工厂未备料', icon: 'component', roles: ['admin'] },
        type: 2,
        children: [
          {
            path: 'fpInBound',
            component: (resolve) => require(['@/views/factoryPreparation/fpInBound'], resolve),
            name: '无单车入库',
            meta: { title: '无单车入库', icon: 'component', roles: ['admin'] },
            type: 3
          },
          {
            path: 'fpOutBound',
            component: (resolve) => require(['@/views/factoryPreparation/fpOutBound'], resolve),
            name: '无单车出库查询',
            meta: { title: '无单车出库查询', icon: 'component', roles: ['admin'] },
            type: 3
          },
          {
            path: 'preparationStock',
            component: (resolve) => require(['@/views/factoryPreparation/preparationStock'], resolve),
            name: '无单车库存',
            meta: { title: '无单车库存', icon: 'component', roles: ['admin'] },
            type: 3
          }]
      }
    ]
  },
  {
    path: '',
    name: '提车管理',
    meta: { title: '提车管理', icon: 'component', roles: ['admin'] },
    component: (resolve) => require(['@/views/layout/Layout'], resolve),
    type: 1,
    children: [
      {
        path: '',
        component: AppMain,
        meta: { title: '指令查询', icon: 'component', roles: ['admin'] },
        type: 2,
        children: [
          {
            path: 'shipment',
            component: (resolve) => require(['@/views/pick/shipment'], resolve),
            name: '指令查询',
            meta: { title: '指令查询', icon: 'component', roles: ['admin'] },
            type: 3
          }]
      },
      {
        path: '',
        component: AppMain,
        type: 2,
        meta: { title: '任务查询', icon: 'component', roles: ['admin'] },
        icon: 'el-icon-news',
        children: [
          {
            path: 'task',
            component: (resolve) => require(['@/views/pick/task'], resolve),
            name: '任务查询',
            meta: { title: '任务查询', icon: 'component', roles: ['admin'] },
            type: 3
          }]
      },
      {
        path: '',
        component: AppMain,
        type: 2,
        meta: { title: '异常查询', icon: 'component', roles: ['admin'] },
        icon: 'el-icon-news',
        children: [
          {
            path: 'exception',
            component: (resolve) => require(['@/views/pick/exception'], resolve),
            name: '异常查询',
            meta: { title: '异常查询', icon: 'component', roles: ['admin'] },
            type: 3
          }]
      }
    ]
  },
  {
    path: '',
    name: '系统设置',
    meta: { title: '系统设置', icon: 'component', roles: ['admin'] },
    component: (resolve) => require(['@/views/layout/Layout'], resolve),
    type: 1,
    children: [
      {
        path: '',
        component: AppMain,
        name: '仓库配置',
        meta: { title: '仓库配置', icon: 'component', roles: ['admin'] },
        type: 2,
        children: [
          {
            path: 'storehouse',
            component: (resolve) => require(['@/views/house/storehouse'], resolve),
            name: '仓库维护',
            meta: { title: '仓库维护', icon: 'component', roles: ['admin'] },
            type: 3
          },
          {
            path: 'storeArea',
            component: (resolve) => require(['@/views/house/storeArea'], resolve),
            name: '库区维护',
            meta: { title: '库区维护', icon: 'component', roles: ['admin'] },
            type: 3
          },
          {
            path: 'storeLocation',
            component: (resolve) => require(['@/views/house/storeLocation'], resolve),
            name: '库位维护',
            meta: { title: '库位维护', icon: 'component', roles: ['admin'] },
            type: 3
          },
          {
            path: 'nodeOption',
            component: (resolve) => require(['@/views/house/nodeOption'], resolve),
            name: '发车点配置',
            meta: { title: '发车点配置', icon: 'component', roles: ['admin'] },
            type: 3
          }
        ]
      },
      {
        path: '',
        component: AppMain,
        name: '用户权限',
        meta: { title: '用户权限', icon: 'component', roles: ['admin'] },
        type: 2,
        children: [
          {
            path: 'role',
            component: (resolve) => require(['@/views/sys/role'], resolve),
            name: '角色管理',
            meta: { title: '角色管理', icon: 'component', roles: ['admin'] },
            type: 3
          },
          {
            path: 'user',
            component: (resolve) => require(['@/views/sys/user'], resolve),
            name: '用户管理',
            meta: { title: '用户管理', icon: 'component', roles: ['admin'] },
            type: 3
          },
          {
            path: 'changePwd',
            component: (resolve) => require(['@/views/sys/changePwd'], resolve),
            name: '修改密码',
            meta: { title: '修改密码', icon: 'component', roles: ['admin'] },
            type: 3
          }
        ]
      },
      {
        path: '',
        component: AppMain,
        type: 2,
        meta: { title: '模版下载', icon: 'component', roles: ['admin'] },
        children: [
          {
            path: 'template',
            component: (resolve) => require(['@/views/sys/template'], resolve),
            name: '模版下载',
            meta: { title: '模版下载', icon: 'component', roles: ['admin'] },
            type: 3
          }]
      },
      {
        path: '',
        component: AppMain,
        type: 2,
        meta: { title: '系统日志', icon: 'component', roles: ['admin'] },
        children: [
          {
            path: 'log',
            component: (resolve) => require(['@/views/sys/log'], resolve),
            name: '系统日志',
            meta: { title: '系统日志', icon: 'component', roles: ['admin'] },
            type: 3
          }]
      }
    ]
  }
  // {
  //   path: '',
  //   name: '系统设置',
  //   component: (resolve) => require(['@/views/layout/Layout'], resolve),
  //   type: 1,
  //   children: [
  //     {
  //       path: 'role',
  //       component: (resolve) => require(['@/views/sys/role'], resolve),
  //       name: '角色管理',
  //       icon: 'el-icon-news'
  //     },
  //     {
  //       path: 'user',
  //       component: (resolve) => require(['@/views/sys/user'], resolve),
  //       name: '用户管理',
  //       icon: 'el-icon-news'
  //     },
  //     {
  //       path: 'changePwd',
  //       component: (resolve) => require(['@/views/sys/changePwd'], resolve),
  //       name: '修改密码',
  //       icon: 'el-icon-edit'
  //     },
  //     {
  //       path: 'template',
  //       component: (resolve) => require(['@/views/sys/template'], resolve),
  //       name: '模版下载',
  //       icon: 'el-icon-download'
  //     },
  //     {
  //       path: 'log',
  //       component: (resolve) => require(['@/views/htmlViewSample'], resolve),
  //       name: '系统日志',
  //       icon: 'el-icon-document'
  //     }
  //   ]
  // },
  // { path: '*', name: 'error', hidden: true, redirect: '/404\'/404\'' }
]

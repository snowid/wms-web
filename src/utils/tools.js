/* eslint-disable */
import { MessageBox, Message } from 'element-ui';

// 获取打印全局打印变量
const LODOP = getLodop(document.getElementById('LODOP_OB'), document.getElementById('LODOP_EM'))
window.LODOP = LODOP
// 检测打印机插件是否安装
export function checkPrintInstall(obj) {
  try {
    // 设置注册码
    const LODOP = window.LODOP
    console.log('LODOP', LODOP)
    console.log('LODOP-Window', window.LODOP)
    if (LODOP && LODOP.VERSION) {
      LODOP.SET_LICENSES('北京知车科技有限责任公司', 'BC324309DFEA0DECE56958C928E1CA08', '', '')
      return true
    } else {
      MessageBox.confirm('该电脑未安装 打印机驱动, 是否下载驱动?', '提示', {
        confirmButtonText: '下载',
        cancelButtonText: '取消',
        type: 'warning'
      }).then(() => {
        const a = document.createElement('a')
        a.href = 'static/plugin/Lodop6.220_CLodop3.020.zip'
        a.click()
        // window.open('static/plugin/Lodop6.220_CLodop3.020.zip')
      }).catch(() => {
        //
      });
      return false
    }
  } catch (e) {
    return false
  }
}

// 打印
export function printHTML(html, width, pages) { // width : 宽度 pages : 页数
  const LODOP = window.LODOP
  if (!width) {
    width = 790
  }
  var html1 = '<html><head><link href="/static/printCss/element-ui.css" type="text/css" rel="stylesheet"><link href="/static/printCss/index.css" type="text/css" rel="stylesheet"></head><body style="width:' + width + 'px;">'
  html += '<style type="text/css">html{padding: 8px;}</style>'
  html1 += html
  html1 += '</body></html>'

  LODOP.PRINT_INIT('打印')
  LODOP.ADD_PRINT_HTM(0, 0, '210mm', '297mm', html1)
  // LODOP.ADD_PRINT_HTM(0, 0, '21mm', '18mm', html1)
  if (pages) {
    // 设置打印数量
    LODOP.SET_PRINT_COPIES(pages)
  }
  LODOP.SET_PRINT_MODE('PRINT_PAGE_PERCENT', 'Width:95%')
  //
  // 调试模式
  LODOP.PREVIEW()

  // 打印模式
  // LODOP.PRINT();
}

export function printQr(base64, vin, localNo, orderBillNo, pages, whCode) {   //打印二维码   localNo 库位 orderBillNo运单号
  const LODOP = window.LODOP
  if (!vin) {
    vin = '';
  }

  if (!localNo) {
    localNo = '';
  }

  if (!orderBillNo) {
    orderBillNo = '';
  }

  var html = '';
  LODOP.PRINT_INIT("二维码打印");
  var tableStart = '<table>';
  var tr0 = '<tr></tr>'
  var tr1 = '<tr><td rowspan="2" style="overflow: hidden"><div class="img-box "><img style="width: 74px;position: absolute;top: -10px;left: -10px;" src="data:image/png;base64,' + base64 + '"/></div></td><td>底盘号:' + vin + '</td></tr>';
  var tr2 = '<tr><td>库位号:' + localNo + '</td></tr>';
  var tr3 = '<tr><td colspan="2" style="font-size: 10px;margin: 0px; padding: 0px;">运单号:' + orderBillNo + '</td></tr>';

  var tableEnd = '</table>';

  var div0 = '<div class="QR-header"><span class="QR-small">运单号:</span><span class="QR-small">'+ orderBillNo +'</span></div>'
  var div1 = '<div><div class="QR-image-left"><img class="QR-image-left-img" src="data:image/png;base64,'+ base64 +'" alt=""></div><div class="QR-image-right"><span class="QR-small">库位号</span><p class="QR-image-right-p">'+ localNo +'</p></div></div><div style="clear: both"></div>'
  var div2 = '<div class="QR-footer"><span class="QR-small-more">vin:</span><span class="QR-small-more">'+ vin +'</span></div>'


  var s = '<html><head><link href="/static/printCss/QR.css" type="text/css" rel="stylesheet"></head><body>'
  var e = '</body></html>'
  // html = s + tableStart + tr1 + tr2 + tr3 + tableEnd + e;
  html = s + div0 + div1 + div2 + e;
  LODOP.SET_PRINT_PAGESIZE(0, 300, 200);
  LODOP.ADD_PRINT_HTM(0, 0, '13mm', '18mm', html);

  if (pages) {
    LODOP.SET_PRINT_COPIES(pages);  //设置打印数量
  }
  // 调试模式*/
  LODOP.PREVIEW();
  // 打印模式
 // LODOP.PRINT();
}
export function printQrImp(base64, vin, localNo, orderBillNo, pages, whCode) {   //打印二维码   localNo 库位 orderBillNo运单号
  const LODOP = window.LODOP
  if (!vin) {
    vin = '';
  }

  if (!localNo) {
    localNo = '';
  }

  let now_orderBillNo = ''
  let now_orderBillNo1 = ''
  if (!orderBillNo) {
    now_orderBillNo = '';
  } else {
    now_orderBillNo = orderBillNo.replace('JY','J')
    // console.log('orderBillNo22 替换jy后', now_orderBillNo)
    // JY191012302-004 => J191012302-004
    // console.log('indexOf('-')orderBillNo', orderBillNo.indexOf('-'), orderBillNo.indexOf('-') !== -1)
    // J191012302-004 => NaN 11
    // now_orderBillNo = (now_orderBillNo.slice(0, now_orderBillNo.indexOf('-')) + now_orderBillNo.slice(now_orderBillNo.indexOf('-') + 1))
    // J191012302-004 => J191012302004
    // console.log('slice', (now_orderBillNo.slice(0, now_orderBillNo.indexOf('-')) + '</br>' + now_orderBillNo.slice(now_orderBillNo.indexOf('-') + 1)))
    // J191012302-004 => J191012302</br>004
    if (orderBillNo.indexOf('-') !== -1) {
      now_orderBillNo1 = now_orderBillNo.slice(0, now_orderBillNo.indexOf('-'))
      // now_orderBillNo2 = now_orderBillNo.slice(now_orderBillNo.indexOf('-') + 1)
    } else {
      now_orderBillNo1 = now_orderBillNo
    }
  }

  var html = '';
  LODOP.PRINT_INIT("二维码打印");

  if (true) {
    var tableStart = '<table border="0" style="font-size: 12px; margin: 0; padding: 0; font-weight:bold;border:none;" width="112px">';
    var tr1 = '<tr><td rowspan="4" style="overflow: hidden"><div style="width:50px;height:50px;overflow:hidden"><img style="margin-right: -8px;margin-top: -8px;" src="data:image/png;base64,' + base64 + '"/></div></td></tr>';
    var tr2 = '<tr><td style="font-size: 10px">' + vin.substring(vin.length-8) + '</td></tr><tr><td>' + localNo + '</td></tr>';
    var tr3 = '<tr><td style="font-size: 10px">' + now_orderBillNo1 + '</td></tr>';
    var tableEnd = '</table>';
    html = tableStart + tr1 + tr2 + tr3 + tableEnd;
    LODOP.SET_PRINT_PAGESIZE(0, 300, 200);
    LODOP.ADD_PRINT_HTM(1, 0, '13mm', '15mm', html);
  } else if (false) {
    var tableStart = '<table border="0" style="font-size: 12px; margin: 0px; padding: 0px; font-weight:bold; height: 100px;">';
    var tr1 = '<tr><td><img style="margin-left: -8px" src="data:image/png;base64,' + base64 + '"/></td></tr>';
    var tr2 = '<tr><td><div style="margin-left: 72px;">' + now_orderBillNo + '</div></td></tr>';
    var tableEnd = '</table>';
    html = tableStart + tr1 + tr2 + tableEnd;
    LODOP.SET_PRINT_PAGESIZE(0, 700, 1000);
    LODOP.ADD_PRINT_HTM(10, 0, '70mm', '100mm', html);
  }

  if (pages) {
    LODOP.SET_PRINT_COPIES(pages);  //设置打印数量
  }
  /*LODOP.PREVIEW(); //调试模式*/
  LODOP.PRINT();  //打印模式
}

export function printQrWithoutImp(vin, localNo, orderBillNo, destAddress, pages, whCode) {   //打印二维码   localNo 库位 orderBillNo运单号
  const LODOP = window.LODOP
  if (!vin) {
    vin = '';
  }

  if (!localNo) {
    localNo = '';
  }

  if (!orderBillNo) {
    orderBillNo = '';
  }

  if (!destAddress) {
    destAddress = '';
  }

  var html = '';
  LODOP.PRINT_INIT("二维码打印");

  if (true) {
    var tableStart = '<table border="0" style="font-size: 10px; margin: 0px; padding: 0px; font-weight:bold;border:none;" width="112px">';
    var tr3 = '<tr><td>' + destAddress + '</td></tr>';
    var tr1 = '<tr><td>' + vin.substring(vin.length-6) + '</td></tr>';
    var tr2 = '<tr><td>' + localNo + '</td></tr>';
    var tr4 = '<tr><td style="font-size: 10px;margin: 0px; padding: 0px;">' + orderBillNo + '</td></tr>';
    var tableEnd = '</table>';
    html = tableStart + tr1 + tr2 + tr3 + tr4 + tableEnd;
    LODOP.SET_PRINT_PAGESIZE(0, 300, 200);
    LODOP.ADD_PRINT_HTM(2, 7, '13mm', '18mm', html);
  } else if (false) {
    var tableStart = '<table border="0" style="font-size: 10px; margin: 0px; padding: 0px; font-weight:bold; height: 100px;">';
    var tr1 = '';
    var tr2 = '<tr><td><div style="margin-left: 72px;">' + destAddress + '</div></td></tr>';
    var tr3 = '<tr><td><div style="margin-left: 72px;">' + orderBillNo + '</div></td></tr>';
    var tableEnd = '</table>';
    html = tableStart + tr1 + tr2 + tr3 + tableEnd;
    LODOP.SET_PRINT_PAGESIZE(0, 700, 1000);
    LODOP.ADD_PRINT_HTM(2, 0, '70mm', '100mm', html);
  }

  if (pages) {
    LODOP.SET_PRINT_COPIES(pages);  //设置打印数量
  }
  /*LODOP.PREVIEW(); //调试模式*/
  LODOP.PRINT();  //打印模式
}
/**
 *  凭借后台返回code 等于 -2时，  错误table
 */
export function errorMessageBox(arr) {
  if (arr instanceof Array) {
    let str = '<table class="errorMessageBox">'
    str += '<tr><th>' + arr[0].split(':')[0] + '</th><th>失败原因</th></tr>'
    for (let i = 0; i < arr.length; i++) {
      const temp = arr[i].split(':')
      str += '<tr><td>' + temp[1] + '</td>'
      str += '<td style="color:red;width: 400px">' + temp[2] + '</td></tr>'
    }
    str += '</table>'
    return str
  } else {
    return arr
  }
}


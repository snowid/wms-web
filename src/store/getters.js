const getters = {
  sidebar: state => state.app.sidebar,
  token: state => state.account.token,
  accountId: state => state.account.accountId,
  avatar: state => state.account.avatar,
  name: state => state.account.name,
  roles: state => state.account.roles,
  storehouses: state => state.account.storehouses,
  currentStorehouse: state => state.account.currentStorehouse,
  entityName: state => state.account.entityName,
  allrouter: state => state.navs.routers,
  nav_routers: state => state.navs.addRouters,
  nav_menus: state => state.navs.navmenus,
  visitedViews: state => state.app.visitedViews,
  cachedViews: state => state.app.cachedViews
}
export default getters

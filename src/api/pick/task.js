import fetch from '@/utils/fetch'

// 任务查询页-列表查询
export function getTaskList(data) {
  return fetch({
    url: '/OpManagement/getTaskList',
    method: 'post',
    data: data
  })
}
// 导出
export function exportTaskData(data) {
  return fetch({
    url: '/OpManagement/exportTaskData',
    method: 'post',
    data: data
  })
}

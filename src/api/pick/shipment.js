import fetch from '@/utils/fetch'

// 指令查询页-列表查询
export function getShipmentList(data) {
  return fetch({
    url: '/OpManagement/getShipmentList',
    method: 'post',
    data: data
  })
}

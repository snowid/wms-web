import fetch from '@/utils/fetch'

export function queryPage(data) {
  return fetch({
    url: '/apSetting/queryPage',
    method: 'post',
    data
  })
}

export function create(data) {
  return fetch({
    url: '/apSetting/create',
    method: 'post',
    data
  })
}

export function update(data) {
  return fetch({
    url: '/apSetting/update',
    method: 'put',
    data
  })
}

export function deleted(data) {
  return fetch({
    url: '/apSetting/deleted',
    method: 'delete',
    data
  })
}

export function getApSetting(id) {
  return fetch({
    url: '/apSetting/get/' + id,
    method: 'get'
  })
}


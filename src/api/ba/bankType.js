import fetch from '@/utils/fetch'
export function queryPage(data) {
  return fetch({
    url: '/bankType/queryListForPage',
    method: 'post',
    data
  })
}

export function create(data) {
  return fetch({
    url: '/bankType/addBankType',
    method: 'post',
    data
  })
}

export function update(data) {
  return fetch({
    url: '/bankType/updateBankType',
    method: 'post',
    data
  })
}

export function deleted(id) {
  return fetch({
    url: '/bankType/delete',
    method: 'get',
    params: { id }
  })
}

export function deletedForBatch(data) {
  return fetch({
    url: '/bankType/deleteForBatch',
    method: 'post',
    data
  })
}

export function getBankTypeList() {
  return fetch({
    url: '/bankType/getBankTypeList',
    method: 'get'
  })
}

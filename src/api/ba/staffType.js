import fetch from '@/utils/fetch'

/**
 *  列表
 * @param data
 */
export function listStaffType(data) {
  return fetch({
    url: '/staffType/list',
    method: 'post',
    data
  })
}

/**
 * 获取人员类别树
 * @param data
 */
export function getStaffTypeTree() {
  return fetch({
    url: '/staffType/getStaffTypeTree',
    method: 'get'
  })
}

/**
 * 新增人员类别
 * @param data
 */
export function addStaffType(data) {
  return fetch({
    url: '/staffType/add',
    method: 'post',
    data
  })
}

/**
 * 获取人员类别
 * @param data
 */
export function getStaffType(id) {
  return fetch({
    url: '/staffType/get/' + id,
    method: 'get'
  })
}

/**
 * 更新人员类别
 * @param data
 */
export function updateStaffType(data) {
  return fetch({
    url: '/staffType/update',
    method: 'put',
    data
  })
}

/**
 * 删除人员类别
 * @param data
 */
export function deleteStaffType(data) {
  return fetch({
    url: '/staffType/delete',
    method: 'delete',
    data
  })
}

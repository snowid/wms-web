import fetch from '@/utils/fetch'
export function queryPage(data) {
  return fetch({
    url: '/stockType/queryListForPage',
    method: 'post',
    data
  })
}

export function create(data) {
  return fetch({
    url: '/stockType/addStockType',
    method: 'post',
    data
  })
}

export function update(data) {
  return fetch({
    url: '/stockType/updateStockType',
    method: 'post',
    data
  })
}

export function deleted(id) {
  return fetch({
    url: '/stockType/delete',
    method: 'get',
    params: { id }
  })
}

export function deletedForBatch(data) {
  return fetch({
    url: '/stockType/deleteForBatch',
    method: 'post',
    data
  })
}

export function getTreeDate() {
  return fetch({
    url: '/stockType/getStockTypeTreeList',
    method: 'get'
  })
}

export function getStockTypeSelect() {
  return fetch({
    url: '/stockType/getStockTypeList',
    method: 'get'
  })
}

import fetch from '@/utils/fetch'

// 出库作业页-列表查询
export function getOutboundShipList(data) {
  return fetch({
    url: '/outboundShipManage/getOutboundShipList',
    method: 'post',
    data: data
  })
}
export function outboundShipConfirm(data) {
  return fetch({
    url: '/outboundShipManage/outboundShipConfirm',
    method: 'post',
    data: data
  })
}
export function getOutShipInfoScan(data) {
  return fetch({
    url: '/outboundShipManage/getOutShipInfoScan',
    method: 'post',
    data: data
  })
}
export function confirmOutboundScan(data) {
  return fetch({
    url: '/outboundShipManage/confirmOutboundScan',
    method: 'post',
    data: data
  })
}
export function getUseAbleLocation(data) {
  return fetch({
    url: '/inboundTask/queryArea',
    method: 'post',
    data: data
  })
}
export function getOutboundShipInfoForQuit(data) {
  return fetch({
    url: '/outboundShipManage/getOutboundShipInfoForQuit',
    method: 'post',
    data: data
  })
}
export function updateOutboundShipQuit(data) {
  return fetch({
    url: '/outboundShipManage/updateOutboundShipQuit',
    method: 'post',
    data: data
  })
}
// 出库记录页-列表查询
export function queryOutShipLine(data) {
  return fetch({
    url: '/outboundShipManage/queryOutboundShip',
    method: 'post',
    data: data
  })
}
// 出库记录导出
export function exportData(data) {
  return fetch({
    url: '/outboundShipManage/exportOSData',
    method: 'post',
    data: data
  })
}
// 发运记录
export function queryShipRecordList(data) {
  return fetch({
    url: '/orderRelease/queryShipRecordList',
    method: 'post',
    data: data
  })
}
// 确认发运(指令维度)
export function confirmShipment(data) {
  return fetch({
    url: '/orderRelease/confirmShipment',
    method: 'post',
    data: data
  })
}
// 确认发运(单车批量)
export function releaseDespatch(data) {
  return fetch({
    url: '/orderRelease/releaseDespatch',
    method: 'post',
    data: data
  })
}
// 二手车发运记录
export function queryNewChannelReleaseList(data) {
  return fetch({
    url: '/orderRelease/queryNewChannelReleaseList',
    method: 'post',
    data: data
  })
}
// 二手车确认发运(单车/批量)
export function despatchNewChannel(data) {
  return fetch({
    url: '/orderRelease/despatchNewChannel',
    method: 'post',
    data: data
  })
}

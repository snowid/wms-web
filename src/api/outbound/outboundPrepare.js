import fetch from '@/utils/fetch'

// 备料出库 -生成备料任务
export function singlePreparation(data) {
  return fetch({
    url: '/preparationManage/singlePreparation',
    method: 'post',
    data: data
  })
}

// 已生成备料列表-列表查询
export function getPreparationList(data) {
  return fetch({
    url: '/preparationManage/getPreparationList',
    method: 'post',
    data: data
  })
}
export function getPreparationDetail(data) {
  return fetch({
    url: '/preparationManage/getPreparationDetail',
    method: 'post',
    data: data
  })
}
export function createPreparation(data) {
  return fetch({
    url: '/preparationManage/createPreparation',
    method: 'post',
    data: data
  })
}
export function changePreparation(data) {
  return fetch({
    url: '/preparationManage/changePreparation',
    method: 'post',
    data: data
  })
}
// 未生成备料列表-列表查询
export function withoutPreparationList(data) {
  return fetch({
    url: '/preparationManage/notPreparationList',
    method: 'post',
    data: data
  })
}

import fetch from '@/utils/fetch'

/**
 *  列表
 * @param data
 */
export function orderImport(header, rows) {
  const data = {
    header,
    rows
  }
  return fetch({
    url: '/inbound/orderImport/importData',
    method: 'post',
    data
  })
}

import fetch from '@/utils/fetch'

// 清理库存页-清理 取消入库库存
export function cleanStock(data) {
  return fetch({
    url: '/stock/cleanStock',
    method: 'post',
    data: data
  })
}

// 清理库存页-主列表查询
export function queryCancleInbound(data) {
  return fetch({
    url: '/inboundTask/queryCancleInbound',
    method: 'post',
    data: data
  })
}

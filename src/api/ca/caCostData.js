import fetch from '@/utils/fetch'

// 分页查询
export function queryPage(data) {
  return fetch({
    url: '/caCostData/queryPage',
    method: 'post',
    data
  })
}

// 新增
export function addCaCostData(data) {
  return fetch({
    url: '/caCostData/addCaCostData',
    method: 'post',
    data
  })
}

// 编辑
export function updateCaCostData(data) {
  return fetch({
    url: '/caCostData/updateCaCostData',
    method: 'post',
    data
  })
}

// 删除
export function deleteForBatch(data) {
  return fetch({
    url: '/caCostData/deleteForBatch',
    method: 'post',
    data
  })
}

// 获取数据
export function getCaCostData(id) {
  return fetch({
    url: '/caCostData/getCaCostData/' + id,
    method: 'get'
  })
}

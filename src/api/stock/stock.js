import fetch from '@/utils/fetch'

// 库位调整页-清理取消入库库存
export function cleanStock(data) {
  return fetch({
    url: '/stock/cleanStock',
    method: 'post',
    data: data
  })
}

// 库位调整页-明细页-列表查询
export function queryStockQtyDetail(data) {
  return fetch({
    url: '/stock/queryStockQtyDetail',
    method: 'post',
    data: data
  })
}

// 库位调整页-列表查询
export function queryStockQty(data) {
  return fetch({
    url: '/stock/queryStockQty',
    method: 'post',
    data: data
  })
}

// 库存管理页- 列表查询
export function queryStock(data) {
  return fetch({
    url: '/stock/queryPage',
    method: 'post',
    data: data
  })
}
// 查询可用库位
export function queryAllArea(data) {
  return fetch({
    url: '/inboundTask/queryArea',
    // url: '/warehouse/queryAllArea',
    method: 'post',
    data: data
  })
}
// 调整库存库位
export function updateStockLocation(data) {
  return fetch({
    url: '/stock/updateStockLocation',
    method: 'post',
    data: data
  })
}
// 锁定库存
export function lockStockForBatch(data) {
  return fetch({
    url: '/stock/lockStock',
    method: 'post',
    data: data
  })
}
// 解锁库存
export function unlockStockForBatch(data) {
  return fetch({
    url: '/stock/unlockStock',
    method: 'post',
    data: data
  })
}
// 库存导出所有
export function exportStockData(data) {
  return fetch({
    url: '/stock/exportStockData',
    method: 'post',
    data: data
  })
}

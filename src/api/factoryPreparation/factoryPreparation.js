import fetch from '@/utils/fetch'

// 查询未备料入库数据
export function queryFactoryInbound(data) {
  return fetch({
    url: '/factoryStock/queryFactoryInbound',
    method: 'post',
    data: data
  })
}

// 查询未备料出库数据
export function queryFactoryOutbound(data) {
  return fetch({
    url: '/factoryStock/queryFactoryOutbound',
    method: 'post',
    data: data
  })
}

// 未备料入库（车架号批量入库，excel导入）
export function importPrepareInbound(data) {
  return fetch({
    url: '/factoryStock/importPrepareInbound',
    method: 'post',
    data: data
  })
}

// 未备料入库（车架号批量入库，数据输入）
export function inboundPrepare(data) {
  return fetch({
    url: '/factoryStock/inboundPrepare',
    method: 'post',
    data: data
  })
}

// 查询仓库-库区
export function queryStoreArea(data) {
  return fetch({
    url: '/storeLocation/queryStoreArea',
    method: 'post',
    data: data
  })
}

// 工厂未备料出库
export function factoryStockOutboundPrepare(data) {
  return fetch({
    url: '/factoryStock/outboundPrepare',
    method: 'post',
    data: data
  })
}

// 工厂未备料库存
export function queryFactoryStock(data) {
  return fetch({
    url: '/stock/queryFactoryStock',
    method: 'post',
    data: data
  })
}

// 首页 必办事项   storeHouseId
export function queryMustMatter(data) {
  return fetch({
    url: '/factoryStock/queryMustMatter',
    method: 'post',
    data: data
  })
}

//  工厂未备料库存页 库位调整
export function updateFactoryLocation(data) {
  return fetch({
    url: '/stock/updateFactoryLocation',
    method: 'post',
    data: data
  })
}

//  工厂未备料库存页 库位列表查询
export function queryMovementArea(data) {
  return fetch({
    url: '/storeLocation/queryMovementArea',
    method: 'post',
    data: data
  })
}

import fetch from '@/utils/fetch'
export function queryPage(data) {
  return fetch({
    url: '/warehouse/listStoreHouse',
    method: 'post',
    data
  })
}
// 从integration 获取仓库基础信息
export function getStoreCode(data) {
  return fetch({
    baseURL: process.env.BASE_API_INTEGRATION,
    url: '/origination/queryPageOrigination ',
    method: 'post',
    data
  })
}
// 获取所有节点信息
export function getNodeOption(data) {
  return fetch({
    url: '/warehouse/listNodeOption ',
    method: 'post',
    data
  })
}
export function create(data) {
  return fetch({
    url: '/warehouse/saveStoreHouse',
    method: 'post',
    data
  })
}
export function update(data) {
  return fetch({
    url: '/warehouse/updateStoreHouse',
    method: 'post',
    data
  })
}
//
// export function deleted(data) {
//   return fetch({
//     url: '/user/delete',
//     method: 'post',
//     data
//   })
// }
//
// export function allRole() {
//   return fetch({
//     url: '/role/all',
//     method: 'get'
//   })
// }

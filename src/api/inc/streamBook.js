import fetch from '@/utils/fetch'

/**
 *  列表
 * @param data
 */
export function listStreamBook(data) {
  return fetch({
    url: '/incStreamBook/list',
    method: 'post',
    data
  })
}

/**
 * 余额账
 */
export function balanceList(data) {
  return fetch({
    url: '/incStreamBook/balanceList',
    method: 'post',
    data
  })
}

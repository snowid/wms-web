import fetch from '@/utils/fetch'

export function listPayInvoice(data) {
  return fetch({
    url: '/payInvoice/list',
    method: 'post',
    data
  })
}

export function getDetail(id) {
  return fetch({
    url: '/payInvoice/getDetail/' + id,
    method: 'get'
  })
}

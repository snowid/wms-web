import fetch from '@/utils/fetch'

export function listPayableOrderLog(data) {
  return fetch({
    url: '/payableOrderLog/list',
    method: 'post',
    data
  })
}

export function getPayableOrderLogHistory(orderCode) {
  return fetch({
    url: '/payableOrderLog/getDetail/' + orderCode,
    method: 'get'
  })
}

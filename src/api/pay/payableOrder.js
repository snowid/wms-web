import fetch from '@/utils/fetch'

// 获取应付发运单
export function getPayableOrderList(data) {
  return fetch({
    url: '/payableOrder/getPayableOrderList',
    method: 'post',
    data
  })
}

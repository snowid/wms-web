import fetch from '@/utils/fetchNoTimeout'

// 手动抓取
export function freshErpOrder(data) {
  return fetch({
    url: '/shouldPayment/freshErpOrder',
    method: 'post',
    data
  })
}

import fetch from '@/utils/fetch'

var qs = require('qs')

export function login(data) {
  return fetch({
    baseURL: process.env.BASE_API_LOGIN,
    url: '/oauth/token',
    method: 'post',
    data,
    header: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

export function getInfo() {
  return fetch({
    url: '/user/info',
    method: 'get'
  })
}

export function logout() {
  return fetch({
    url: '/user/logout',
    method: 'post'
  })
}

export function examinPassword(password) {
  return fetch({
    url: '/account/examinPassword',
    method: 'get',
    params: { password }
  })
}

export function updatePwd(password) {
  return fetch({
    url: '/account/updatePwd',
    method: 'post',
    data: qs.stringify({
      password
    })
  })
}

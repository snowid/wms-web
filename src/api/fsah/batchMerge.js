import fetch from '@/utils/fetchNoTimeout'

// 重新生成分录
export function mergeToCreateAndUpload(data) {
  return fetch({
    url: '/batchJourna/mergeToCreateAndUpload',
    method: 'post',
    data
  })
}

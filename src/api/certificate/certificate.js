import fetch from '@/utils/fetch'

// 合格证管理页-导出
export function exportCertificationPage(data) {
  return fetch({
    url: '/certification/exportCertificationPage',
    method: 'post',
    data: data
  })
}

// 合格证管理页-列表查询
export function queryCertificationPage(data) {
  return fetch({
    url: '/certification/queryCertificationPage',
    method: 'post',
    data: data
  })
}

// 合格证管理页- 领取 发放更新
export function updateCertification(data) {
  return fetch({
    url: '/certification/updateCertification',
    method: 'post',
    data: data
  })
}

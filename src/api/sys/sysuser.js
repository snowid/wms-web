import fetch from '@/utils/fetch'

// 用户管理页- type 禁用 0 or 启用 1
export function disableUserInfo(data) {
  return fetch({
    url: '/user/disableUserInfo',
    method: 'post',
    data
  })
}

// 用户管理页-发运点分页查询
export function allUsablePoint(data) {
  return fetch({
    url: '/deliveryPoint/allUsablePoint',
    method: 'post',
    data
  })
}

// 用户管理页-仓库分页查询
export function allUsableStoreHouse(data) {
  return fetch({
    url: '/warehouse/allUsableStoreHouse',
    method: 'post',
    data
  })
}

export function queryPage(data) {
  return fetch({
    url: '/user/queryPage',
    method: 'post',
    data
  })
}

export function create(data) {
  return fetch({
    url: '/user/create',
    method: 'post',
    data
  })
}

export function update(data) {
  return fetch({
    url: '/user/update',
    method: 'post',
    data
  })
}

export function deleted(data) {
  return fetch({
    url: '/user/delete',
    method: 'post',
    data
  })
}

export function allRole() {
  return fetch({
    url: '/role/all',
    method: 'get'
  })
}

export function ownRole(userId) {
  return fetch({
    url: '/user/ownRole',
    method: 'get',
    params: { userId }
  })
}

export function grantRole(userId, roleIds) {
  const data = {
    userId,
    roleIds
  }
  return fetch({
    url: '/user/grantRole',
    method: 'post',
    data
  })
}

export function allHouse() {
  return fetch({
    url: '/warehouse/allUsableStoreHouse',
    method: 'get'
  })
}

export function ownHouse(userId) {
  return fetch({
    url: '/user/ownHouses',
    method: 'get',
    params: { userId }
  })
}

export function grantHouse(userId, houseIds) {
  const data = {
    userId,
    houseIds
  }
  return fetch({
    url: '/user/grantHouse',
    method: 'post',
    data
  })
}

export function allPoint() {
  return fetch({
    url: '/deliveryPoint/allUsablePoint',
    method: 'post'
  })
}

export function ownPoint(userId) {
  return fetch({
    url: '/user/listOwnPoints',
    method: 'get',
    params: { userId }
  })
}

export function grantPoint(userId, points) {
  const data = {
    userId,
    points
  }
  return fetch({
    url: '/user/grantPoint',
    method: 'post',
    data
  })
}

var merge = require('webpack-merge')
var prodEnv = require('./prod.env')

// 菜花 BASE_API: '"http://10.2.7.116:8090"',
// 晓辉 BASE_API: '"http://10.2.6.250:8090/admin"',
// BASE_API: '"http://uat.unlcn.com/wms-admin/"',
// BASE_API_LOGIN: '"http://10.20.30.111:8090"',
// BASE_API_LOGIN:'"https://lisa-test.huiyunche.cn/uaa"'
module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  BASE_API_INTEGRATION: '"http://10.20.30.102:8890/lisa-integration"',
  BASE_API: '"http://10.2.7.116:8090"',
  BASE_API_LOGIN: '"http://10.20.30.111:8090/uaa"'
})
